package pl.grzegorz.keycloakserviceserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@ActiveProfiles("test")
@Testcontainers
@AutoConfigureMockMvc
@SpringBootTest
public abstract class AbstractIntegrationTest {

    private static final Network INTEGRATION_TEST_NETWORK = Network.newNetwork();
    private static final String POSTGRES_IMAGE = "postgres:14-alpine";
    private static final String KEYCLOAK_IMAGE = "quay.io/keycloak/keycloak:22.0.1";
    private static final String KEYCLOAK_SERVICE_TEST_DB = "keycloak-service_test_db";
    private static final String KEYCLOAK_SERVICE_TEST_DB_USER = "keycloak-service";
    private static final String KEYCLOAK_SERVICE_TEST_DB_PASSWORD = "keycloak-service";
    private static final String KEYCLOAK_SERVICE_DB_ALIAS = "keycloak-service-db";
    private static final String KEYCLOAK_TEST_DB = "keycloak_db";
    private static final String KEYCLOAK_ALIAS = "keycloak-db";
    private static final String KEYCLOAK_TEST_DB_USER = "keycloak_user";
    private static final String KEYCLOAK_TEST_DB_PASSWORD = "keycloak_password";
    private static final String KEYCLOAK_START_COMMAND = "start-dev";
    private static final int KEYCLOAK_PORT = 8080;
    private static final String KEYCLOAK_DB_ENGINE = "postgres";
    private static final String KEYCLOAK_DB_SCHEMA = "public";
    private static final String KEYCLOAK_ADMIN = "admin";
    private static final String KEYCLOAK_PASSWORD = "password";

    private static final PostgreSQLContainer<?> KEYCLOAK_SERVICE_POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_IMAGE))
            .withDatabaseName(KEYCLOAK_SERVICE_TEST_DB)
            .withPassword(KEYCLOAK_SERVICE_TEST_DB_PASSWORD)
            .withUsername(KEYCLOAK_SERVICE_TEST_DB_USER)
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(KEYCLOAK_SERVICE_DB_ALIAS);

    private static final PostgreSQLContainer<?> KEYCLOAK_POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(
            DockerImageName.parse(POSTGRES_IMAGE))
            .withDatabaseName(KEYCLOAK_TEST_DB)
            .withPassword(KEYCLOAK_TEST_DB_PASSWORD)
            .withUsername(KEYCLOAK_TEST_DB_USER)
            .withNetwork(INTEGRATION_TEST_NETWORK)
            .withNetworkAliases(KEYCLOAK_ALIAS);

    private static final GenericContainer<?> KEYCLOAK_CONTAINER = new GenericContainer<>(
            DockerImageName.parse(KEYCLOAK_IMAGE)
    )
            .withEnv("KC_DB", KEYCLOAK_DB_ENGINE)
            .withEnv("KC_DB_URL", String.format("jdbc:postgresql://%s:5432/%s", KEYCLOAK_ALIAS, KEYCLOAK_TEST_DB))
            .withEnv("KC_DB_DATABASE", KEYCLOAK_TEST_DB)
            .withEnv("KC_DB_USERNAME", KEYCLOAK_TEST_DB_USER)
            .withEnv("KC_DB_PASSWORD", KEYCLOAK_TEST_DB_PASSWORD)
            .withEnv("KD_DB_SCHEMA", KEYCLOAK_DB_SCHEMA)
            .withEnv("KEYCLOAK_ADMIN", KEYCLOAK_ADMIN)
            .withEnv("KEYCLOAK_ADMIN_PASSWORD", KEYCLOAK_PASSWORD)
            .withCommand(KEYCLOAK_START_COMMAND)
            .withExposedPorts(KEYCLOAK_PORT)
            .withNetwork(INTEGRATION_TEST_NETWORK);

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ObjectMapper mapper;

    static {
        KEYCLOAK_SERVICE_POSTGRE_SQL_CONTAINER.start();
        KEYCLOAK_POSTGRE_SQL_CONTAINER.start();
        KEYCLOAK_CONTAINER.start();
        System.setProperty("keycloak.service.url", String.format("http://localhost:%s",
                KEYCLOAK_CONTAINER.getMappedPort(KEYCLOAK_PORT)));
    }

    @DynamicPropertySource
    public static void postgresContainerConfig(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", KEYCLOAK_SERVICE_POSTGRE_SQL_CONTAINER::getJdbcUrl);
        registry.add("spring.datasource.password", KEYCLOAK_SERVICE_POSTGRE_SQL_CONTAINER::getPassword);
        registry.add("spring.datasource.username", KEYCLOAK_SERVICE_POSTGRE_SQL_CONTAINER::getUsername);
    }
}