package pl.grzegorz.keycloakserviceserver.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.grzegorz.keycloakapplication.ports.in.*;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerCreateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerDisableCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerEnableCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerUpdateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.*;
import pl.grzegorz.keycloakapplication.services.*;

@Configuration
@EnableJpaRepositories(basePackages = {"pl.grzegorz"})
@EntityScan(basePackages = {"pl.grzegorz"})
class KeycloakServiceBeans {

    @Bean
    public Jackson2JsonMessageConverter jsonMessageConverter() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        return new Jackson2JsonMessageConverter(objectMapper);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory() {
        return new SimpleRabbitListenerContainerFactory();
    }

    @Bean
    public UserCreateCommandUseCase userCreateCommandUseCase(UserQueryPort userQueryPort,
                                                             UserCreateCommandPort userCreateCommandPort,
                                                             BikerCreateCommandPort bikerCreateCommandPort) {
        return new UserCreateCommandService(userQueryPort, userCreateCommandPort, bikerCreateCommandPort);
    }

    @Bean
    public UserCreatePasswordCommandUseCase userCreatePasswordCommandUseCase(
            UserCreatePasswordCommandPort userCreatePasswordCommandPort) {
        return new UserCreatePasswordCommandService(userCreatePasswordCommandPort);
    }

    @Bean
    public UserDisableCommandUseCase userDisableCommandUseCase(UserDisableCommandPort userDisableCommandPort,
                                                               BikerDisableCommandPort bikerDisableCommandPort) {
        return new UserDisableCommandService(userDisableCommandPort, bikerDisableCommandPort);
    }

    @Bean
    public UserUpdateCommandUseCase userUpdateCommandUseCase(UserQueryPort userQueryPort,
                                                             UserUpdateCommandPort userUpdateCommandPort,
                                                             BikerUpdateCommandPort bikerUpdateCommandPort) {
        return new UserUpdateCommandService(userQueryPort, userUpdateCommandPort, bikerUpdateCommandPort);
    }

    @Bean
    public UserEnableCommandUseCase userEnableCommandUseCase(UserEnableCommandPort userEnableCommandPort,
                                                             BikerEnableCommandPort bikerEnableCommandPort) {
        return new UserEnableCommandService(userEnableCommandPort, bikerEnableCommandPort);
    }
}