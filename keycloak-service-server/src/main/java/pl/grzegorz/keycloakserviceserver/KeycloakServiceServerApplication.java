package pl.grzegorz.keycloakserviceserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"pl.grzegorz"})
@ConfigurationPropertiesScan(basePackages = {"pl.grzegorz"})
public class KeycloakServiceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeycloakServiceServerApplication.class, args);
    }

}