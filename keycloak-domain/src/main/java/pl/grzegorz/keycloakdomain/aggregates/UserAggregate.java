package pl.grzegorz.keycloakdomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.keycloakdomain.data.keycloakuser.KeycloakUserUpdateData;
import pl.grzegorz.keycloakdomain.data.keycloakuser.UserCreateData;

import java.time.LocalDate;
import java.util.UUID;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class UserAggregate {

    private UUID id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean enable;
    private String phoneNumber;
    private LocalDate dateOfBirth;

    public void setId(UUID id) {
        this.id = id;
    }

    public static UserAggregate create(UserCreateData createData) {
        return UserAggregate.builder()
                .withUsername(createData.username())
                .withFirstName(createData.firstName())
                .withLastName(createData.lastName())
                .withEmail(createData.email())
                .withEnable(Boolean.FALSE)
                .withPhoneNumber(createData.phoneNumber())
                .withDateOfBirth(createData.dateOfBirth())
                .build();
    }

    public UserAggregate update(KeycloakUserUpdateData keycloakUserUpdateData) {
        return UserAggregate.builder()
                .withFirstName(keycloakUserUpdateData.firstName())
                .withLastName(keycloakUserUpdateData.lastName())
                .withEmail(keycloakUserUpdateData.email())
                .withEnable(Boolean.TRUE)
                .withPhoneNumber(keycloakUserUpdateData.phoneNumber())
                .build();
    }
}