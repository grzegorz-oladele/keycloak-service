package pl.grzegorz.keycloakdomain.aggregates;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import pl.grzegorz.keycloakdomain.data.credential.PasswordCreateData;

@Getter
@Accessors(fluent = true)
@Builder(toBuilder = true, setterPrefix = "with")
public class CredentialAggregate {

    private static final String CREDENTIAL_TYPE = "password";

    private String password;
    private String keycloakUserLabel;
    private Integer priority;
    private String type;
    private Boolean temporary;

    public static CredentialAggregate create(PasswordCreateData passwordCreateData) {
        return CredentialAggregate.builder()
                .withPassword(passwordCreateData.password())
                .withKeycloakUserLabel(passwordCreateData.keycloakUserLabel())
                .withType(CREDENTIAL_TYPE)
                .withTemporary(passwordCreateData.temporary())
                .build();
    }
}