package pl.grzegorz.keycloakdomain.exception;

public class KeycloakUserNotFoundException extends DomainException{
    public KeycloakUserNotFoundException(String message) {
        super(message);
    }
}