package pl.grzegorz.keycloakdomain.exception;

public class KeycloakUserEnableException extends DomainException {
    public KeycloakUserEnableException(String message) {
        super(message);
    }
}
