package pl.grzegorz.keycloakdomain.exception.messages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ExceptionMessage {

    KEYCLOAK_USER_NOT_FOUND_ERROR("User with id %s not found"),
    KEYCLOAK_USER_ALREADY_EXISTS_BY_USERNAME_ERROR("Keycloak user with username [%s] already exists"),
    KEYCLOAK_USER_ALREADY_EXISTS_BY_ID_ERROR("Keycloak user with id [%s] already exists"),
    KEYCLOAK_USER_ENABLE_ERROR("Keycloak user with id [%s] is enable right now"),
    KEYCLOAK_USER_DISABLE_ERROR("Keycloak user with id [%s] is disable right now"),
    KEYCLOAK_CLIENT_EXCEPTION_MESSAGE("Keycloak client for keycloak-service in keycloak instance not found. You have to " +
            "create new client in keycloak using client id -> [%s]"),
    CLIENT_REPRESENTATION_NOT_FOUND_EXCEPTION_MESSAGE("Client with clientId [%s] not found ");

    private final String message;
}