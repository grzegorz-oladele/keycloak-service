package pl.grzegorz.keycloakdomain.exception;

public class KeycloakUserAlreadyExistsException extends DomainException{
    public KeycloakUserAlreadyExistsException(String message) {
        super(message);
    }
}
