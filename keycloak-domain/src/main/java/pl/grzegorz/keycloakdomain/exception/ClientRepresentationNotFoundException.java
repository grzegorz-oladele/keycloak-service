package pl.grzegorz.keycloakdomain.exception;

public class ClientRepresentationNotFoundException extends DomainException {

    public ClientRepresentationNotFoundException(String message) {
        super(message);
    }
}
