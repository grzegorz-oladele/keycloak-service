package pl.grzegorz.keycloakdomain.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
abstract class DomainException extends RuntimeException {

    private final String message;
}