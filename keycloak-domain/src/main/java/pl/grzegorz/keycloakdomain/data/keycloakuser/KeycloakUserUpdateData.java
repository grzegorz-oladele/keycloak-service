package pl.grzegorz.keycloakdomain.data.keycloakuser;

import lombok.Builder;

@Builder(toBuilder = true, setterPrefix = "with")
public record KeycloakUserUpdateData(
        String username,
        String firstName,
        String lastName,
        String email,
        String phoneNumber,
        String dateOfBirth
) {
}