package pl.grzegorz.keycloakdomain.data.keycloakuser;

import lombok.Builder;

import java.time.LocalDate;

@Builder(toBuilder = true, setterPrefix = "with")
public record UserCreateData(
        String username,
        String firstName,
        String lastName,
        String email,
        String phoneNumber,
        LocalDate dateOfBirth
) {
}
