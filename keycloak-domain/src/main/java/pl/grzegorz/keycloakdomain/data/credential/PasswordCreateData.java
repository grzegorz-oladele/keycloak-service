package pl.grzegorz.keycloakdomain.data.credential;

import lombok.Builder;

@Builder(toBuilder = true, setterPrefix = "with")
public record PasswordCreateData(
        String password,
        String keycloakUserLabel,
        Boolean temporary
) {
}