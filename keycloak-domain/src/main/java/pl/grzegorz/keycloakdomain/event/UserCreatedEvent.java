package pl.grzegorz.keycloakdomain.event;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record UserCreatedEvent(
        UUID id,
        String username,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber,
        String dateOfBirth
) implements DomainEvent {

    public static UserCreatedEvent of(UserAggregate userAggregate) {
        return UserCreatedEvent.builder()
                .withId(userAggregate.id())
                .withUsername(userAggregate.username())
                .withFirstName(userAggregate.firstName())
                .withLastName(userAggregate.lastName())
                .withEmail(userAggregate.email())
                .withEnable(userAggregate.enable())
                .withPhoneNumber(userAggregate.phoneNumber())
                .withDateOfBirth(userAggregate.dateOfBirth().toString())
                .build();
    }
}