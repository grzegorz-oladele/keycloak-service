package pl.grzegorz.keycloakdomain.event;

import lombok.AccessLevel;
import lombok.Builder;

import java.util.UUID;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record UserEnabledEvent(
        UUID id
) implements DomainEvent {

    public static UserEnabledEvent of(String bikerId) {
        return UserEnabledEvent.builder()
                .withId(UUID.fromString(bikerId))
                .build();
    }
}