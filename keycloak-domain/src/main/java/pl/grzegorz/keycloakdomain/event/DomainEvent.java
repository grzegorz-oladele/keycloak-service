package pl.grzegorz.keycloakdomain.event;

import java.util.UUID;

public interface DomainEvent {
    UUID id();
}