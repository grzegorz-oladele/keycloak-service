package pl.grzegorz.keycloakdomain.event;

import lombok.AccessLevel;
import lombok.Builder;

import java.util.UUID;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record UserDisabledEvent(
        UUID id
) implements DomainEvent {

    public static UserDisabledEvent of(String userId) {
        return UserDisabledEvent.builder()
                .withId(UUID.fromString(userId))
                .build();
    }
}