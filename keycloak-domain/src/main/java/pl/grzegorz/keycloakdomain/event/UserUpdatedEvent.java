package pl.grzegorz.keycloakdomain.event;

import lombok.AccessLevel;
import lombok.Builder;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

@Builder(setterPrefix = "with", access = AccessLevel.PRIVATE)
public record UserUpdatedEvent(
        UUID id,
        String firstName,
        String lastName,
        String email,
        Boolean enable,
        String phoneNumber
) implements DomainEvent {

    public static UserUpdatedEvent of(String bikerId, UserAggregate userAggregate) {
        return UserUpdatedEvent.builder()
                .withId(UUID.fromString(bikerId))
                .withFirstName(userAggregate.firstName())
                .withLastName(userAggregate.lastName())
                .withEmail(userAggregate.email())
                .withEnable(userAggregate.enable())
                .withPhoneNumber(userAggregate.phoneNumber())
                .build();
    }
}