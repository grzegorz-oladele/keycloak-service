package pl.grzegorz.keycloakdomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.keycloakdomain.data.keycloakuser.KeycloakUserUpdateData;
import pl.grzegorz.keycloakdomain.data.keycloakuser.UserCreateData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;
import static pl.grzegorz.keycloakdomain.Fixtures.*;

class UserAggregateTest {

    private UserAggregate userAggregate;
    private UserCreateData userCreateData;
    private KeycloakUserUpdateData keycloakUserUpdateData;

    @BeforeEach
    void setup() {
        userAggregate = userAggregate();
        userCreateData = userCreateData();
        keycloakUserUpdateData = keycloakUserUpdateData();
    }

    @Test
    void shouldCreateNewUserAggregateObject() {
//        given
//        when
        var aggregate = UserAggregate.create(userCreateData);
//        then
        assertAll(
                () -> assertThat(aggregate.username(), is(userCreateData.username())),
                () -> assertThat(aggregate.firstName(), is(userCreateData.firstName())),
                () -> assertThat(aggregate.lastName(), is(userCreateData.lastName())),
                () -> assertThat(aggregate.email(), is(userCreateData.email())),
                () -> assertThat(aggregate.enable(), is(Boolean.FALSE)),
                () -> assertThat(aggregate.phoneNumber(), is(userCreateData.phoneNumber())),
                () -> assertThat(aggregate.dateOfBirth(), is(userCreateData.dateOfBirth()))
        );
    }

    @Test
    void shouldUpdateUserAggregateObject() {
//        given
//        when
        var updatedAggregate= userAggregate.update(keycloakUserUpdateData);
//        then
        assertAll(
                () -> assertThat(updatedAggregate.firstName(), is(keycloakUserUpdateData.firstName())),
                () -> assertThat(updatedAggregate.lastName(), is(keycloakUserUpdateData.lastName())),
                () -> assertThat(updatedAggregate.email(), is(keycloakUserUpdateData.email())),
                () -> assertThat(updatedAggregate.enable(), is(Boolean.TRUE)),
                () -> assertThat(updatedAggregate.phoneNumber(), is(keycloakUserUpdateData.phoneNumber()))
        );
    }
}