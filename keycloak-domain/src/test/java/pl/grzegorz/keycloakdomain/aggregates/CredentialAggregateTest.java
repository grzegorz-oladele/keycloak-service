package pl.grzegorz.keycloakdomain.aggregates;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.grzegorz.keycloakdomain.data.credential.PasswordCreateData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static pl.grzegorz.keycloakdomain.Fixtures.passwordCreateData;

class CredentialAggregateTest {

    private PasswordCreateData passwordCreateData;

    @BeforeEach
    void setup() {
        passwordCreateData = passwordCreateData();
    }

    @Test
    void shouldCreatePassword() {
//        given
//        when
        var aggregate = CredentialAggregate.create(passwordCreateData);
//        then
        assertAll(
                () -> assertThat(aggregate.password(), is(passwordCreateData.password())),
                () -> assertThat(aggregate.keycloakUserLabel(), is(passwordCreateData.keycloakUserLabel())),
                () -> assertThat(aggregate.temporary(), is(passwordCreateData.temporary()))
        );
    }
}