package pl.grzegorz.keycloakdomain;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.data.credential.PasswordCreateData;
import pl.grzegorz.keycloakdomain.data.keycloakuser.KeycloakUserUpdateData;
import pl.grzegorz.keycloakdomain.data.keycloakuser.UserCreateData;

import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    public static UserAggregate userAggregate() {
        return UserAggregate.builder()
                .withUsername("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@email.pl")
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1987,4,21))
                .build();
    }

    public static UserCreateData userCreateData() {
        return UserCreateData.builder()
                .withUsername("tomasz_123")
                .withFirstName("Tomasz")
                .withLastName("Tomaszewski")
                .withEmail("tomasz@email.pl")
                .withPhoneNumber("123-456-789")
                .withDateOfBirth(LocalDate.of(1987,4,21))
                .build();
    }

    public static KeycloakUserUpdateData keycloakUserUpdateData() {
        return KeycloakUserUpdateData.builder()
                .withUsername("adrian")
                .withFirstName("Adrian")
                .withLastName("Adrianowski")
                .withEmail("adrian@email.pl")
                .withPhoneNumber("459-036-384")
                .withDateOfBirth(LocalDate.of(1987,4,21).toString())
                .build();
    }

    public static PasswordCreateData passwordCreateData() {
        return PasswordCreateData.builder()
                .withPassword("password")
                .withKeycloakUserLabel("PASSWORD")
                .withTemporary(Boolean.FALSE)
                .build();
    }
}