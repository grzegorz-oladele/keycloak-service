# Keycloak-Service



## Description
This microservice serves as a platform for user creation and is tightly integrated with the 
[biker-service](https://gitlab.com/grzegorz-oladele/biker-micoservice). Each operation that affects the database state 
is handled by sending the appropriate request - asynchronously through the RabbitMQ message queue system or 
synchronously using the FeignClient HTTP client - to the biker-service, ensuring data consistency between the two 
services. The microservice establishes a connection to Keycloak using the keycloak-admin-client library while exposing 
a comprehensive API to facilitate CRUD operations as well as user activation and deactivation.

## Technologies Used
- [ ] Spring Boot 3.x.x
- [ ] Hibernate
- [ ] PostgreSQL
- [ ] Keycloak
- [ ] RabbitMQ

## Example Usage
- [ ] Endpoint: <span style="color:orange">POST</span>  /users
    - Description: Add new user to the application
    - Request Body: Provide the necessary user information
    - Response: Returns the ID of the newly created user
- [ ] Endpoint: <span style="color:purple">PATCH</span> /users/`{userId}`/credentials
    - Description: Assign a password and disable a user
    - Parameters: `{userId}` - The ID of the biker
    - Request Body: Provide the necessary user password information
- [ ] Endpoint: <span style="color:purple">PATCH</span> /users/`{userId}`/disable
    - Description: Disable a user
    - Parameters: `{userId}` - The ID of the biker
- [ ] Endpoint: <span style="color:green">GET</span> /users/`{userId}`/exists
    - Description: Check if the user is in the datab
    - Response: True if user exists, False if user does not exist
- [ ] Endpoint: <span style="color:purple">PATCH</span> /users/`{userId}`/update
    - Description: Update user
    - Parameters: `{userId}` - The ID of the biker
- [ ] Endpoint: <span style="color:purple">PATCH</span> /users/`{userId}`/credentials/update
    - Description: Update password
    - Parameters: `{userId}` - The ID of the biker
    - Parameters: credentials - List of credentials

## Architecture
The project follows Domain-Driven Design (DDD) and event storming principles. It consists of four modules:
- [ ] **Server**: Responsible for application startup, integration tests, and management of environment variables
- [ ] **Domain**: The core module that contains key objects and dedicated methods for working with those objects
- [ ] **Application**: Exposes application use cases
- [ ] **Adapters**: Handles communication with external systems and other microservices within the cluster

## Project Status
The keycloak-microservice is actively being actively developed. We are constantly working on adding new features including:
- [ ] implementation of the CQRS pattern
- [ ] addition and implementation of Liquibase library
- [ ] implementation of security using the Spring Security framework
- [ ] increasing test coverage of key application functionalities

# Author
**Grzegorz Oladele**

Thank you for your interest in the keycloak-microservice project! If you have any questions or feedback, please
feel free to contact us.
