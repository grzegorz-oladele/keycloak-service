package pl.grzegorz.keycloakapplication.services;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.keycloakapplication.ports.in.UserCreatePasswordCommandUseCase;
import pl.grzegorz.keycloakapplication.ports.out.user.UserCreatePasswordCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.CredentialAggregate;

@RequiredArgsConstructor
public class UserCreatePasswordCommandService implements UserCreatePasswordCommandUseCase {

    private final UserCreatePasswordCommandPort userCreatePasswordCommandPort;

    @Override
    public void create(String userId, PasswordCreateCommand passwordCreateCommand) {
        userCreatePasswordCommandPort.createPassword(userId,
                CredentialAggregate.create(passwordCreateCommand.toCreateData()));
    }
}