package pl.grzegorz.keycloakapplication.services;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.keycloakapplication.ports.in.UserUpdateCommandUseCase;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerUpdateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserQueryPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserUpdateCommandPort;

@RequiredArgsConstructor
public class UserUpdateCommandService implements UserUpdateCommandUseCase {

    private final UserQueryPort userQueryPort;
    private final UserUpdateCommandPort userUpdateCommandPort;
    private final BikerUpdateCommandPort bikerUpdateCommandPort;

    @Override
    public void update(String keycloakUserId, KeycloakUserUpdateCommand keycloakUserUpdateCommand) {
        var userAggregate = userQueryPort.getKeycloakUserById(keycloakUserId);
        var updatedUserAggregate = userAggregate.update(keycloakUserUpdateCommand.toUpdateData());
        userUpdateCommandPort.update(keycloakUserId, updatedUserAggregate);
        bikerUpdateCommandPort.publish(keycloakUserId, updatedUserAggregate);
    }
}