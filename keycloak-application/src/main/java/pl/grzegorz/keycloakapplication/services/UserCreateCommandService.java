package pl.grzegorz.keycloakapplication.services;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.keycloakapplication.ports.in.UserCreateCommandUseCase;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerCreateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserCreateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserQueryPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

@RequiredArgsConstructor
public class UserCreateCommandService implements UserCreateCommandUseCase {

    private final UserQueryPort userQueryPort;
    private final UserCreateCommandPort userCreateCommandPort;
    private final BikerCreateCommandPort bikerCreateCommandPort;

    @Override
    public UUID create(UserCreateCommand userCreateCommand) {
        userQueryPort.verifyExistsUserInKeycloakByUsername(userCreateCommand.username());
        var userAggregate = UserAggregate.create(userCreateCommand.toCreateData());
        var keycloakUserId = userCreateCommandPort.create(userAggregate);
        userAggregate.setId(keycloakUserId);
        bikerCreateCommandPort.publish(userAggregate);
        return userAggregate.id();
    }
}