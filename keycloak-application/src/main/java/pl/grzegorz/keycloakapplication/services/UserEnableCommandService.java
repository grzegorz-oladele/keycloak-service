package pl.grzegorz.keycloakapplication.services;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.keycloakapplication.ports.in.UserEnableCommandUseCase;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerEnableCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserEnableCommandPort;

@RequiredArgsConstructor
public class UserEnableCommandService implements UserEnableCommandUseCase {

    private final UserEnableCommandPort userEnableCommandPort;
    private final BikerEnableCommandPort bikerEnableCommandPort;

    @Override
    public void enable(String userId) {
        userEnableCommandPort.enable(userId);
        bikerEnableCommandPort.publish(userId);
    }
}