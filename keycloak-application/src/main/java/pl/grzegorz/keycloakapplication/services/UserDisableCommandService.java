package pl.grzegorz.keycloakapplication.services;

import lombok.RequiredArgsConstructor;
import pl.grzegorz.keycloakapplication.ports.in.UserDisableCommandUseCase;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerDisableCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserDisableCommandPort;

@RequiredArgsConstructor
public class UserDisableCommandService implements UserDisableCommandUseCase {

    private final UserDisableCommandPort userDisableCommandPort;
    private final BikerDisableCommandPort bikerDisableCommandPort;

    @Override
    public void disable(String userId) {
        userDisableCommandPort.disable(userId);
        bikerDisableCommandPort.publish(userId);
    }
}