package pl.grzegorz.keycloakapplication.ports.out.biker;

import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

public interface BikerCreateCommandPort {

    void publish(UserAggregate userAggregate);
}