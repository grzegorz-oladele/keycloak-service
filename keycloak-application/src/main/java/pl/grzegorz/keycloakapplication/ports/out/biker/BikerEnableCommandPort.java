package pl.grzegorz.keycloakapplication.ports.out.biker;

public interface BikerEnableCommandPort {

    void publish(String bikerId);
}