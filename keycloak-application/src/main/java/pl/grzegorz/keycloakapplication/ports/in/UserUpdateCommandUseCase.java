package pl.grzegorz.keycloakapplication.ports.in;

import lombok.Builder;
import pl.grzegorz.keycloakdomain.data.keycloakuser.KeycloakUserUpdateData;

public interface UserUpdateCommandUseCase {

    void update(String keycloakUserId, KeycloakUserUpdateCommand keycloakUserUpdateCommand);

    @Builder(toBuilder = true, setterPrefix = "with")
    record KeycloakUserUpdateCommand(
            String firstName,
            String lastName,
            String email,
            String phoneNumber,
            String dateOfBirth
    ) {
        public KeycloakUserUpdateData toUpdateData() {
            return KeycloakUserUpdateData.builder()
                    .withFirstName(firstName)
                    .withLastName(lastName)
                    .withEmail(email)
                    .withPhoneNumber(phoneNumber)
                    .withDateOfBirth(dateOfBirth)
                    .build();
        }
    }
}