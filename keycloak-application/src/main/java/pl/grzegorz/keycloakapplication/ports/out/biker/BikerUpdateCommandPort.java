package pl.grzegorz.keycloakapplication.ports.out.biker;

import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

public interface BikerUpdateCommandPort {

    void publish(String bikerId, UserAggregate userAggregate);
}