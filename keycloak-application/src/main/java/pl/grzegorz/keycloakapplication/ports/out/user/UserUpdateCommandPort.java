package pl.grzegorz.keycloakapplication.ports.out.user;

import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

public interface UserUpdateCommandPort {

    void update(String userId, UserAggregate userAggregate);
}