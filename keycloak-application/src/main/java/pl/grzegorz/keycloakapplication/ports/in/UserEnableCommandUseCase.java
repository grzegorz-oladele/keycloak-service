package pl.grzegorz.keycloakapplication.ports.in;

public interface UserEnableCommandUseCase {

    void enable(String userId);
}