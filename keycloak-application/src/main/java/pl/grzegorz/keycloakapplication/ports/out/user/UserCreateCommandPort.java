package pl.grzegorz.keycloakapplication.ports.out.user;

import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

public interface UserCreateCommandPort {

    UUID create(UserAggregate userAggregate);
}