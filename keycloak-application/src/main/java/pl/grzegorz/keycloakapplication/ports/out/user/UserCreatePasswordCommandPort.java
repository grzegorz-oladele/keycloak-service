package pl.grzegorz.keycloakapplication.ports.out.user;

import pl.grzegorz.keycloakdomain.aggregates.CredentialAggregate;

public interface UserCreatePasswordCommandPort {

    void createPassword(String userId, CredentialAggregate credentialAggregate);
}