package pl.grzegorz.keycloakapplication.ports.out.user;

import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

public interface UserQueryPort {

    UserAggregate getKeycloakUserById(String userId);

    void verifyExistsUserInKeycloakByUsername(String username);
}