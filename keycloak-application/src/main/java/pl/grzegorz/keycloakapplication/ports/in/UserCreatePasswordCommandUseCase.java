package pl.grzegorz.keycloakapplication.ports.in;

import lombok.Builder;
import pl.grzegorz.keycloakdomain.data.credential.PasswordCreateData;

public interface UserCreatePasswordCommandUseCase {

    void create(String userId, PasswordCreateCommand passwordCreateCommand);

    @Builder(toBuilder = true, setterPrefix = "with")
    record PasswordCreateCommand(
            String password,
            String keycloakUserLabel,
            Boolean temporary
    ) {
        public PasswordCreateData toCreateData() {
            return PasswordCreateData.builder()
                    .withPassword(password)
                    .withKeycloakUserLabel(keycloakUserLabel)
                    .withTemporary(temporary)
                    .build();
        }
    }
}