package pl.grzegorz.keycloakapplication.ports.in;

public interface UserDisableCommandUseCase {

    void disable(String userId);
}