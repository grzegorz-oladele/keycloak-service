package pl.grzegorz.keycloakapplication.ports.out.user;

public interface UserRemoveCommandPort {

    void remove(String userId);
}