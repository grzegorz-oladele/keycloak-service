package pl.grzegorz.keycloakapplication.ports.in;

import lombok.Builder;
import pl.grzegorz.keycloakdomain.data.keycloakuser.UserCreateData;

import java.time.LocalDate;
import java.util.UUID;

public interface UserCreateCommandUseCase {

    UUID create(UserCreateCommand userCreateCommand);

    @Builder(toBuilder = true, setterPrefix = "with")
    record UserCreateCommand(
            String username,
            String firstName,
            String lastName,
            String email,
            Boolean emailVerified,
            String phoneNumber,
            String dateOfBirth
    ) {
        public UserCreateData toCreateData() {
            return UserCreateData.builder()
                    .withUsername(username)
                    .withFirstName(firstName)
                    .withLastName(lastName)
                    .withEmail(email)
                    .withPhoneNumber(phoneNumber)
                    .withDateOfBirth(LocalDate.parse(dateOfBirth))
                    .build();
        }
    }
}