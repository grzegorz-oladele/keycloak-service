package pl.grzegorz.keycloakapplication.ports.out.user;

public interface UserDisableCommandPort {

    void disable(String userId);
}