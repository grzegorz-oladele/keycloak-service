package pl.grzegorz.keycloakapplication.ports.out.user;

public interface UserEnableCommandPort {

    void enable(String userId);
}