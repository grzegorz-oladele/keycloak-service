package pl.grzegorz.keycloakapplication.ports.out.biker;

public interface BikerDisableCommandPort {

    void publish(String userId);
}