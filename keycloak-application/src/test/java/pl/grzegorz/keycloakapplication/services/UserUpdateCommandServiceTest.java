package pl.grzegorz.keycloakapplication.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerUpdateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserQueryPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserUpdateCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.grzegorz.keycloakapplication.ports.in.UserUpdateCommandUseCase.KeycloakUserUpdateCommand;
import static pl.grzegorz.keycloakapplication.services.ApplicationFixtures.*;

@ExtendWith(MockitoExtension.class)
class UserUpdateCommandServiceTest {

    @InjectMocks
    private UserUpdateCommandService userUpdateCommandService;
    @Mock
    private UserQueryPort userQueryPort;
    @Mock
    private UserUpdateCommandPort userUpdateCommandPort;
    @Mock
    private BikerUpdateCommandPort bikerUpdateCommandPort;

    private KeycloakUserUpdateCommand keycloakUserUpdateCommand;
    private UserAggregate userAggregate;

    @BeforeEach
    void setup() {
        keycloakUserUpdateCommand = keycloakUserUpdateCommand();
        userAggregate = userAggregate();
    }

    @Test
    void shouldCall_update_from_UserUpdateCommandPort_and_update_from_BikerUpdateCommandPort() {
//        given
        var keycloakUserId = AGGREGATE_ID.toString();
        when(userQueryPort.getKeycloakUserById(keycloakUserId)).thenReturn(userAggregate);
//        when
        userUpdateCommandService.update(keycloakUserId, keycloakUserUpdateCommand);
//        then
        assertAll(
                () -> verify(userUpdateCommandPort).update(eq(keycloakUserId), any(UserAggregate.class)),
                () -> verify(bikerUpdateCommandPort).publish(eq(keycloakUserId), any(UserAggregate.class))
        );
    }
}