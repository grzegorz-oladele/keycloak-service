package pl.grzegorz.keycloakapplication.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerDisableCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserDisableCommandPort;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.keycloakapplication.services.ApplicationFixtures.AGGREGATE_ID;

@ExtendWith(MockitoExtension.class)
class UserDisableCommandServiceTest {

    @InjectMocks
    private UserDisableCommandService userDisableCommandService;
    @Mock
    private UserDisableCommandPort userDisableCommandPort;
    @Mock
    private BikerDisableCommandPort bikerDisableCommandPort;

    @Test
    void shouldCall_disable_from_UserDisableCommandPort_and_disable_from_BikerDisableCommandPort() {
//        given
        var userId = AGGREGATE_ID.toString();
//        when
        userDisableCommandService.disable(userId);
//        then
        assertAll(
                () -> verify(userDisableCommandPort).disable(userId),
                () -> verify(bikerDisableCommandPort).publish(userId)
        );
    }
}