package pl.grzegorz.keycloakapplication.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerEnableCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserEnableCommandPort;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.keycloakapplication.services.ApplicationFixtures.AGGREGATE_ID;

@ExtendWith(MockitoExtension.class)
class UserEnableCommandServiceTest {

    @InjectMocks
    private UserEnableCommandService userEnableCommandService;
    @Mock
    private UserEnableCommandPort userEnableCommandPort;
    @Mock
    private BikerEnableCommandPort bikerEnableCommandPort;

    @Test
    void shouldCall_disable_from_UserDisableCommandPort_and_disable_from_BikerDisableCommandPort() {
//        given
        var userId = AGGREGATE_ID.toString();
//        when
        userEnableCommandService.enable(userId);
//        then
        assertAll(
                () -> verify(userEnableCommandPort).enable(userId),
                () -> verify(bikerEnableCommandPort).publish(userId)
        );
    }
}