package pl.grzegorz.keycloakapplication.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.keycloakapplication.ports.out.user.UserCreatePasswordCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.CredentialAggregate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static pl.grzegorz.keycloakapplication.ports.in.UserCreatePasswordCommandUseCase.PasswordCreateCommand;
import static pl.grzegorz.keycloakapplication.services.ApplicationFixtures.AGGREGATE_ID;

@ExtendWith(MockitoExtension.class)
class UserCreatePasswordCommandServiceTest {

    @InjectMocks
    private UserCreatePasswordCommandService userCreatePasswordCommandService;
    @Mock
    private UserCreatePasswordCommandPort userCreatePasswordCommandPort;

    private PasswordCreateCommand passwordCreateCommand;

    @BeforeEach
    void setup() {
        passwordCreateCommand = ApplicationFixtures.passwordCreateCommand();
    }

    @Test
    void shouldCall_createPassword_from_BikerEnableCommandPort_and_enable_from_BikerEnableCommandPort() {
//        given
        var userId = AGGREGATE_ID.toString();
//        when
        userCreatePasswordCommandService.create(userId, passwordCreateCommand);
//        then
        verify(userCreatePasswordCommandPort).createPassword(eq(userId), any(CredentialAggregate.class));
    }
}