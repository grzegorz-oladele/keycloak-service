package pl.grzegorz.keycloakapplication.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerCreateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserCreateCommandPort;
import pl.grzegorz.keycloakapplication.ports.out.user.UserQueryPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

import static org.mockito.Mockito.*;
import static pl.grzegorz.keycloakapplication.ports.in.UserCreateCommandUseCase.UserCreateCommand;
import static pl.grzegorz.keycloakapplication.services.ApplicationFixtures.AGGREGATE_ID;
import static pl.grzegorz.keycloakapplication.services.ApplicationFixtures.userCreateCommand;

@ExtendWith(MockitoExtension.class)
class UserCreateCommandServiceTest {

    @InjectMocks
    private UserCreateCommandService userCreateCommandService;
    @Mock
    private UserQueryPort userQueryPort;
    @Mock
    private UserCreateCommandPort userCreateCommandPort;
    @Mock
    private BikerCreateCommandPort bikerCreateCommandPort;

    private UserCreateCommand userCreateCommand;

    @BeforeEach
    void setup() {
        userCreateCommand = userCreateCommand();
    }


    @Test
    void shouldCall_create_fromBikerCreateCommandPort() {
//        given
        var userName = userCreateCommand.username();
        doNothing().when(userQueryPort).verifyExistsUserInKeycloakByUsername(userName);
        lenient().when(userCreateCommandPort.create(any(UserAggregate.class))).thenReturn(AGGREGATE_ID);
//        when
        userCreateCommandService.create(userCreateCommand);
//        then
        verify(bikerCreateCommandPort).publish(any(UserAggregate.class));
    }
}