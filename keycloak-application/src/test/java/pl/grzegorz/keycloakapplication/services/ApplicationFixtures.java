package pl.grzegorz.keycloakapplication.services;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.time.LocalDate;
import java.util.UUID;

import static pl.grzegorz.keycloakapplication.ports.in.UserCreateCommandUseCase.UserCreateCommand;
import static pl.grzegorz.keycloakapplication.ports.in.UserCreatePasswordCommandUseCase.PasswordCreateCommand;
import static pl.grzegorz.keycloakapplication.ports.in.UserUpdateCommandUseCase.KeycloakUserUpdateCommand;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class ApplicationFixtures {

    public static final UUID AGGREGATE_ID = UUID.fromString("fb489a0c-a28d-4e02-b5c5-8f928370715f");
    private static final String USERNAME = "test-username";
    private static final String FIRST_NAME = "test-firstName";
    private static final String LAST_NAME = "test-lastName";
    private static final String EMAIL = "test-email";
    private static final String PHONE_NUMBER = "123-456-789";
    private static final String DATE_OF_BIRTH = LocalDate.now().toString();
    private static final String PASSWORD = "test-password";
    private static final String KEYCLOAK_USER_LABEL = "keycloak_user_label";

    static UserCreateCommand userCreateCommand() {
        return UserCreateCommand.builder()
                .withUsername(USERNAME)
                .withFirstName(FIRST_NAME)
                .withLastName(LAST_NAME)
                .withEmail(EMAIL)
                .withEmailVerified(Boolean.TRUE)
                .withPhoneNumber(PHONE_NUMBER)
                .withDateOfBirth(DATE_OF_BIRTH)
                .build();
    }

    static PasswordCreateCommand passwordCreateCommand() {
        return PasswordCreateCommand.builder()
                .withPassword(PASSWORD)
                .withTemporary(Boolean.TRUE)
                .withKeycloakUserLabel(KEYCLOAK_USER_LABEL)
                .build();
    }

    static KeycloakUserUpdateCommand keycloakUserUpdateCommand() {
        return KeycloakUserUpdateCommand.builder()
                .withFirstName(FIRST_NAME)
                .withLastName(LAST_NAME)
                .withEmail(EMAIL)
                .withPhoneNumber(PHONE_NUMBER)
                .withDateOfBirth(DATE_OF_BIRTH)
                .build();
    }

    static UserAggregate userAggregate() {
        return UserAggregate.builder()
                .withId(AGGREGATE_ID)
                .build();
    }
}