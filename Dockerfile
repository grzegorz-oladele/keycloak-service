FROM amazoncorretto:17-alpine
EXPOSE 8500
COPY keycloak-service-server/target/keycloak-service-server-0.0.1-SNAPSHOT.jar keycloak-service-server-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "keycloak-service-server-0.0.1-SNAPSHOT.jar"]