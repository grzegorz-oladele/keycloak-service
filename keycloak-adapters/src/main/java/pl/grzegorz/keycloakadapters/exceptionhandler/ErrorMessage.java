package pl.grzegorz.keycloakadapters.exceptionhandler;

import lombok.AccessLevel;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder(toBuilder = true, access = AccessLevel.PRIVATE, setterPrefix = "with")
record ErrorMessage(
        String message,
        String path,
        int responseCode,
        LocalDateTime timestamp
) {

    static ErrorMessage getErrorMessage(String message, String path, int responseCode) {
        return ErrorMessage.builder()
                .withMessage(message)
                .withPath(path)
                .withResponseCode(responseCode)
                .withTimestamp(LocalDateTime.now())
                .build();
    }
}