package pl.grzegorz.keycloakadapters.exceptionhandler;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserAlreadyExistsException;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserEnableException;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserNotFoundException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

import javax.ws.rs.NotAuthorizedException;

@RestControllerAdvice
@RequiredArgsConstructor
class GlobalExceptionHandler {

    private final HttpServletRequest httpServletRequest;

    @Value("${keycloak.client-id}")
    private String keycloakServiceClientId;

    @ExceptionHandler(KeycloakUserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ErrorMessage handleKeycloakUserNotFoundException(KeycloakUserNotFoundException exception) {
        return ErrorMessage.getErrorMessage(exception.getMessage(), getCurrentUrlPath(), HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler({KeycloakUserAlreadyExistsException.class, KeycloakUserEnableException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    ErrorMessage handleKeycloakUserAlreadyExistsException(Exception exception) {
        return ErrorMessage.getErrorMessage(exception.getMessage(), getCurrentUrlPath(), HttpStatus.CONFLICT.value());
    }

    @ExceptionHandler(NotAuthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    ErrorMessage handleKeycloakNotAuthorizedException() {
        return ErrorMessage.getErrorMessage(
                String.format(ExceptionMessage.KEYCLOAK_CLIENT_EXCEPTION_MESSAGE.getMessage(), keycloakServiceClientId),
                getCurrentUrlPath(), HttpStatus.UNAUTHORIZED.value());
    }

    private String getCurrentUrlPath() {
        return httpServletRequest.getServletPath();
    }
}