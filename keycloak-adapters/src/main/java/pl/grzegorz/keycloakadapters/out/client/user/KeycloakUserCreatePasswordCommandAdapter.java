package pl.grzegorz.keycloakadapters.out.client.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakadapters.out.client.user.mapper.KeycloakUserMapper;
import pl.grzegorz.keycloakapplication.ports.out.user.UserCreatePasswordCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.CredentialAggregate;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserEnableException;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserNotFoundException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

import javax.ws.rs.NotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
class KeycloakUserCreatePasswordCommandAdapter implements UserCreatePasswordCommandPort {

    private final KeycloakProvider keycloakProvider;
    private final KeycloakUserMapper keycloakUserMapper;

    @Value("${keycloak.moto-app-realm}")
    private String realmName;

    @Override
    public void createPassword(String userId, CredentialAggregate credentialAggregate) {
        try {
            var userResource = keycloakProvider.keycloak()
                    .realm(realmName)
                    .users()
                    .get(userId);
            var userRepresentation = userResource.toRepresentation();
            validateDisableUser(userRepresentation, userId);
            var credentialRepresentation = keycloakUserMapper.toCredential(credentialAggregate);
            userRepresentation.setCredentials(List.of(credentialRepresentation));
            userResource.update(userRepresentation);
            log.info("Assign password to user with id -> [{}]", userId);
        } catch (NotFoundException e) {
            log.error("Keycloak user using id -> [{}] not found", userId);
            throw new KeycloakUserNotFoundException(
                    String.format(ExceptionMessage.KEYCLOAK_USER_NOT_FOUND_ERROR.getMessage(), userId)
            );
        }
    }

    private void validateDisableUser(UserRepresentation userRepresentation, String userId) {
        if (Boolean.FALSE.equals(userRepresentation.isEnabled())) {
            throw new KeycloakUserEnableException(String.format(
                    ExceptionMessage.KEYCLOAK_USER_DISABLE_ERROR.getMessage(), userId
            ));
        }
    }
}