package pl.grzegorz.keycloakadapters.out.client.configuration;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.grzegorz.keycloakdomain.exception.ClientRepresentationNotFoundException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
class ClientRoleConfig {

    private static final String CLIENT_ROLE_NAME = "client_user";

    @Value("${keycloak.moto-app-realm}")
    private String motoAppRealm;
    @Value("${keycloak.moto-app-client-id}")
    private String motoAppClientId;

    public void initializeClientRoles(Keycloak keycloak) {
        var motoUserManager = getMotoUserManagerClient(keycloak)
                .orElseThrow(() -> new ClientRepresentationNotFoundException(String.format(
                        ExceptionMessage.CLIENT_REPRESENTATION_NOT_FOUND_EXCEPTION_MESSAGE.getMessage(), motoAppClientId
                )));
        List<RoleRepresentation> clientRoles = keycloak.realm(motoAppRealm).clients()
                .get(motoUserManager.getId()).roles().list();
        if (validateClientRoleExists(clientRoles)) {
            log.info("Role {} already exists in {} client", CLIENT_ROLE_NAME, motoAppClientId);
        } else {
            createClientRole(keycloak, motoUserManager);
            log.info("Create {} for {} client", CLIENT_ROLE_NAME, motoAppClientId);
        }
    }

    private boolean validateClientRoleExists(List<RoleRepresentation> clientRoles) {
        boolean isExists = false;
        for (RoleRepresentation role : clientRoles) {
            if (role.getName().equals(CLIENT_ROLE_NAME)) {
                isExists = true;
                break;
            }
        }
        return isExists;
    }

    private void createClientRole(Keycloak keycloak, ClientRepresentation motoUserManager) {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        roleRepresentation.setName(CLIENT_ROLE_NAME);
        roleRepresentation.setClientRole(Boolean.TRUE);
        RolesResource rolesResource = keycloak.realm(motoAppRealm).clients().get(motoUserManager.getId()).roles();
        rolesResource.create(roleRepresentation);
    }

    private Optional<ClientRepresentation> getMotoUserManagerClient(Keycloak keycloak) {
        List<ClientRepresentation> clientsRepresentation = keycloak.realms().realm(motoAppRealm).clients().findAll();
        for (ClientRepresentation clientRepresentation : clientsRepresentation) {
            if (clientRepresentation.getClientId().equals(motoAppClientId)) {
                return Optional.of(clientRepresentation);
            }
        }
        return Optional.empty();
    }
}