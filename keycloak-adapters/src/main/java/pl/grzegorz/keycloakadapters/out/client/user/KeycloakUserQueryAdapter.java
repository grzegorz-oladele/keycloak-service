package pl.grzegorz.keycloakadapters.out.client.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakadapters.out.client.user.mapper.KeycloakUserMapper;
import pl.grzegorz.keycloakapplication.ports.out.user.UserQueryPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserAlreadyExistsException;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserEnableException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

@Service
@RequiredArgsConstructor
@Slf4j
class KeycloakUserQueryAdapter implements UserQueryPort {

    private final KeycloakProvider keycloakProvider;
    private final KeycloakUserMapper keycloakUserMapper;

    @Value("${keycloak.moto-app-realm}")
    private String realmName;

    @Override
    public UserAggregate getKeycloakUserById(String userId) {
        var userResource = keycloakProvider.keycloak().realm(realmName).users();
        var userRepresentation = userResource.get(userId).toRepresentation();
        checkUserEnableStatus(userId, userRepresentation);
        return keycloakUserMapper.toUserAggregate(userRepresentation);
    }

    private void checkUserEnableStatus(String userId, UserRepresentation userRepresentation) {
        if (Boolean.FALSE.equals(userRepresentation.isEnabled())) {
            log.error("User using id -> [{}] is disable", userId);
            throw new KeycloakUserEnableException(
                    String.format(ExceptionMessage.KEYCLOAK_USER_DISABLE_ERROR.getMessage(), userId)
            );
        }
    }

    @Override
    public void verifyExistsUserInKeycloakByUsername(String username) {
        var resource = keycloakProvider.keycloak().realm(realmName).users();
        if (!resource.search(username).isEmpty()) {
            log.error("Keycloak user with id -> [{}] does not exists", username);
            throw new KeycloakUserAlreadyExistsException(
                    String.format(ExceptionMessage
                            .KEYCLOAK_USER_ALREADY_EXISTS_BY_USERNAME_ERROR.getMessage(), username)
            );
        }
    }
}