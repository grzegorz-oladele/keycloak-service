package pl.grzegorz.keycloakadapters.out.client.configuration;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
class RealmRolesConfig {

    private static final String REALM_ROLE_NAME = "user";

    @Value("${keycloak.moto-app-realm}")
    private String motoAppRealm;

    public void initializeRealmRoles(Keycloak keycloak) {
        List<RoleRepresentation> realmRoles = keycloak.realm(motoAppRealm).roles().list();
        if (validateRealmRoleExist(realmRoles)) {
            log.info("Role {} already exists in realm {}", REALM_ROLE_NAME, motoAppRealm);
        } else {
            createRealmRole(keycloak);
            log.info("Role {} was created in {} realm", REALM_ROLE_NAME, motoAppRealm);
        }
    }

    private void createRealmRole(Keycloak keycloak) {
        RoleRepresentation roleRepresentation = new RoleRepresentation();
        roleRepresentation.setName(REALM_ROLE_NAME);
        RolesResource rolesResource = keycloak.realms().realm(motoAppRealm).roles();
        rolesResource.create(roleRepresentation);
    }

    private boolean validateRealmRoleExist(List<RoleRepresentation> rolesInRealm) {
        boolean isExists = false;
        for (RoleRepresentation role : rolesInRealm) {
            if (role.getName().equals(REALM_ROLE_NAME)) {
                isExists = true;
                break;
            }
        }
        return isExists;
    }
}
