package pl.grzegorz.keycloakadapters.out.amqp.dto;

import lombok.Builder;

import java.util.UUID;

@Builder(toBuilder = true, setterPrefix = "with")
public record BikerUpdateDto(
        UUID id,
        String firstName,
        String lastName,
        String email,
        String dateOfBirth,
        String phoneNumber
) {
}
