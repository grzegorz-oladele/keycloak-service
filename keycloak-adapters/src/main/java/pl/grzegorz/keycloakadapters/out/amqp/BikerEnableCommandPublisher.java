package pl.grzegorz.keycloakadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerEnableCommandPort;
import pl.grzegorz.keycloakdomain.event.UserEnabledEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerEnableCommandPublisher implements BikerEnableCommandPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.enable.exchange}")
    private String enableBikerExchange;
    @Value("${spring.rabbitmq.biker.enable.routing-key}")
    private String enableBikerRoutingKey;


    @Override
    public void publish(String bikerId) {
        var enabledEvent = UserEnabledEvent.of(bikerId);
        rabbitTemplate.convertAndSend(enableBikerExchange, enableBikerRoutingKey, enabledEvent);
        log.info("Enable biker using id -> [{}] in biker-service", bikerId);
    }
}