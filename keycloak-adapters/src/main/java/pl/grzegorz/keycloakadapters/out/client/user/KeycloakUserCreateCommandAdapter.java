package pl.grzegorz.keycloakadapters.out.client.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakadapters.out.client.user.mapper.KeycloakUserMapper;
import pl.grzegorz.keycloakapplication.ports.out.user.UserCreateCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserAlreadyExistsException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

import javax.ws.rs.core.Response;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
class KeycloakUserCreateCommandAdapter implements UserCreateCommandPort {

    private final KeycloakProvider keycloakProvider;
    private final KeycloakUserMapper keycloakUserMapper;

    @Value("${keycloak.moto-app-realm}")
    private String realmName;

    @Override
    public UUID create(UserAggregate userAggregate) {
        var userRepresentation = keycloakUserMapper.toUserRepresentation(userAggregate);
        var usersResource = keycloakProvider.keycloak().realm(realmName).users();
        var response = usersResource.create(userRepresentation);
        handle409KeycloakResponseCode(userAggregate, response);
        log.info("Create new user in keycloak database with username -> [{}]", userAggregate.username());
        return UUID.fromString(usersResource.search(userAggregate.username()).get(0).getId());
    }

    private void handle409KeycloakResponseCode(UserAggregate userAggregate, Response response) {
        if (response.getStatus() == 409) {
            log.error("Failed to register user in Keycloak database. Keycloak user already exists");
            throw new KeycloakUserAlreadyExistsException(
                    String.format(ExceptionMessage.KEYCLOAK_USER_ALREADY_EXISTS_BY_USERNAME_ERROR.getMessage(),
                            userAggregate.username())
            );
        }
    }
}