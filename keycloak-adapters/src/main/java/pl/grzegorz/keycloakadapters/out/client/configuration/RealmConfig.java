package pl.grzegorz.keycloakadapters.out.client.configuration;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmsResource;
import org.keycloak.representations.idm.RealmRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
class RealmConfig {

    @Value("${keycloak.moto-app-realm}")
    private String motoAppRealm;

    public void initializeRealms(Keycloak keycloak) {
        List<RealmRepresentation> realms = keycloak.realms().findAll();
        if (validateRealmExists(realms)) {
            log.info("Realm {} already exists", motoAppRealm);
        } else {
            log.info("Realm {} created", motoAppRealm);
            createRealm(keycloak);
        }
    }

    private void createRealm(Keycloak keycloak) {
        RealmRepresentation realm = new RealmRepresentation();
        realm.setRealm(motoAppRealm);
        realm.setEnabled(Boolean.TRUE);
        realm.setDisplayName(motoAppRealm);
        RealmsResource realmsResource = keycloak.realms();
        realmsResource.create(realm);
    }

    private boolean validateRealmExists(List<RealmRepresentation> realms) {
        boolean isExists = false;
        for (RealmRepresentation realm : realms) {
            if (realm.getRealm().equals(motoAppRealm)) {
                isExists = true;
                break;
            }
        }
        return isExists;
    }
}