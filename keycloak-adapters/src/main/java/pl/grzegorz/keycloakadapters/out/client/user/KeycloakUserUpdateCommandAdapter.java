package pl.grzegorz.keycloakadapters.out.client.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakadapters.out.client.user.mapper.KeycloakUserMapper;
import pl.grzegorz.keycloakapplication.ports.out.user.UserUpdateCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

@Service
@RequiredArgsConstructor
@Slf4j
class KeycloakUserUpdateCommandAdapter implements UserUpdateCommandPort {

    private final KeycloakProvider keycloakProvider;
    private final KeycloakUserMapper keycloakUserMapper;

    @Value("${keycloak.moto-app-realm}")
    private String realmName;

    @Override
    public void update(String userId, UserAggregate userAggregate) {
        var updatedUserRepresentation = keycloakUserMapper.toUpdateUserRepresentation(userAggregate);
        var userResource = keycloakProvider.keycloak()
                .realm(realmName)
                .users()
                .get(userId);
        userResource.update(updatedUserRepresentation);
        log.info("Update keycloak user with id -> [{}]", userId);
    }
}