package pl.grzegorz.keycloakadapters.out.client.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakapplication.ports.out.user.UserDisableCommandPort;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserEnableException;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserNotFoundException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

import javax.ws.rs.NotFoundException;

@Service
@RequiredArgsConstructor
@Slf4j
class KeycloakUserDisableCommandAdapter implements UserDisableCommandPort {

    private final KeycloakProvider keycloakProvider;

    @Value("${keycloak.moto-app-realm}")
    private String realmName;

    @Override
    public void disable(String userId) {
        try {
            var usersResource = keycloakProvider.keycloak()
                    .realm(realmName)
                    .users();
            var singleUserResource = usersResource.get(userId);
            var userRepresentation = singleUserResource.toRepresentation();
            validateDisableUser(userId, userRepresentation);
            userRepresentation.setEnabled(Boolean.FALSE);
            singleUserResource.update(userRepresentation);
        } catch (NotFoundException e) {
            log.error("Keycloak user using id -> [{}] not found", userId);
            throw new KeycloakUserNotFoundException(
                    String.format(ExceptionMessage.KEYCLOAK_USER_NOT_FOUND_ERROR.getMessage(), userId)
            );
        }
        log.info("Disable keycloak user with id -> [{}]", userId);
    }

    private void validateDisableUser(String userId, UserRepresentation userRepresentation) {
        if (userRepresentation.isEnabled().equals(Boolean.FALSE)) {
            throw new KeycloakUserEnableException(String.format(
                    ExceptionMessage.KEYCLOAK_USER_DISABLE_ERROR.getMessage(), userId
            ));
        }
    }
}