package pl.grzegorz.keycloakadapters.out.client.user.mapper;

import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Component;
import pl.grzegorz.keycloakdomain.aggregates.CredentialAggregate;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.util.UUID;

@Component
public class KeycloakUserMapper {

    public UserRepresentation toUserRepresentation(UserAggregate userAggregate) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(userAggregate.username());
        userRepresentation.setFirstName(userAggregate.firstName());
        userRepresentation.setLastName(userAggregate.lastName());
        userRepresentation.setEmail(userAggregate.email());
        userRepresentation.setEmailVerified(Boolean.TRUE);
        userRepresentation.setEnabled(userAggregate.enable());
        return userRepresentation;
    }

    public UserRepresentation toUpdateUserRepresentation(UserAggregate userAggregate) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setUsername(userAggregate.username());
        userRepresentation.setFirstName(userAggregate.firstName());
        userRepresentation.setLastName(userAggregate.lastName());
        userRepresentation.setEmail(userAggregate.email());
        userRepresentation.setEmailVerified(Boolean.TRUE);
        userRepresentation.setEnabled(Boolean.TRUE);
        return userRepresentation;
    }

    public UserAggregate toUserAggregate(UserRepresentation userRepresentation) {
        return UserAggregate.builder()
                .withId(UUID.fromString(userRepresentation.getId()))
                .withUsername(userRepresentation.getUsername())
                .withFirstName(userRepresentation.getFirstName())
                .withLastName(userRepresentation.getLastName())
                .withEmail(userRepresentation.getEmail())
                .withEnable(userRepresentation.isEnabled())
                .build();
    }

    public CredentialRepresentation toCredential(CredentialAggregate credentialAggregate) {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(credentialAggregate.type());
        credentialRepresentation.setValue(credentialAggregate.password());
        credentialRepresentation.setUserLabel(credentialAggregate.keycloakUserLabel());
        credentialRepresentation.setTemporary(credentialAggregate.temporary());
        credentialRepresentation.setPriority(10);
        return credentialRepresentation;
    }
}