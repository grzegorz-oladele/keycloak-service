package pl.grzegorz.keycloakadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerCreateCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.event.UserCreatedEvent;

@Service
@Slf4j
@RequiredArgsConstructor
class BikerCreateCommand implements BikerCreateCommandPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.create.exchange}")
    private String bikerCreateExchange;
    @Value("${spring.rabbitmq.biker.create.routing-key}")
    private String bikerCreateRoutingKey;

    @Override
    public void publish(UserAggregate userAggregate) {
        var event = UserCreatedEvent.of(userAggregate);
        rabbitTemplate.convertAndSend(bikerCreateExchange, bikerCreateRoutingKey, event);
        log.info("Send event -> [{}] to exchange -> [{}]", event, bikerCreateExchange);
    }
}