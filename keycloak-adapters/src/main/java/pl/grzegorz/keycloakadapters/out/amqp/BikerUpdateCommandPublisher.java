package pl.grzegorz.keycloakadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerUpdateCommandPort;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.event.UserUpdatedEvent;

@Service
@Slf4j
@RequiredArgsConstructor
class BikerUpdateCommandPublisher implements BikerUpdateCommandPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.update.exchange}")
    private String bikerUpdateExchange;
    @Value("${spring.rabbitmq.biker.update.routing-key}")
    private String bikerUpdateRoutingKey;

    @Override
    public void publish(String bikerId, UserAggregate userAggregate) {
        var event = UserUpdatedEvent.of(bikerId, userAggregate);
        rabbitTemplate.convertAndSend(bikerUpdateExchange, bikerUpdateRoutingKey, event);
        log.info("Send event -> [{}] to exchange -> [{}]", event, bikerUpdateExchange);
    }
}