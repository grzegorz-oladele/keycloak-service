package pl.grzegorz.keycloakadapters.out.client.configuration;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
class ClientConfig {

    @Value("${keycloak.moto-app-realm}")
    private String motoAppRealm;
    @Value("${keycloak.moto-app-client-id}")
    private String motoAppClientId;

    public void initializeClients(Keycloak keycloak) {
        List<ClientRepresentation> clients = keycloak.realm(motoAppRealm).clients().findAll();
        if (validateClientExists(clients)) {
            log.info("Client {} already exists", motoAppClientId);
        } else {
            createClient(keycloak);
            log.info("Create {} client in {} realm", motoAppClientId, motoAppRealm);
        }
    }

    private boolean validateClientExists(List<ClientRepresentation> clients) {
        boolean isExists = false;
        for (ClientRepresentation client : clients) {
            if (client.getClientId().equals(motoAppClientId)) {
                isExists = true;
                break;
            }
        }
        return isExists;
    }

    private void createClient(Keycloak keycloak) {
        ClientRepresentation clientRepresentation = new ClientRepresentation();
        clientRepresentation.setEnabled(Boolean.TRUE);
        clientRepresentation.setClientId(motoAppClientId);
        clientRepresentation.setName(motoAppClientId);
        ClientsResource clientsResource = keycloak.realms().realm(motoAppRealm).clients();
        clientsResource.create(clientRepresentation);
    }
}