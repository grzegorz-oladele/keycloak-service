package pl.grzegorz.keycloakadapters.out.client.configuration;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.springframework.stereotype.Component;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakadapters.out.client.configuration.ClientConfig;
import pl.grzegorz.keycloakadapters.out.client.configuration.ClientRoleConfig;
import pl.grzegorz.keycloakadapters.out.client.configuration.RealmConfig;
import pl.grzegorz.keycloakadapters.out.client.configuration.RealmRolesConfig;

@Component
@RequiredArgsConstructor
@Slf4j
class Setup {

    private final KeycloakProvider keycloakProvider;
    private final RealmConfig realmConfig;
    private final ClientConfig clientConfig;
    private final RealmRolesConfig realmRolesConfig;
    private final ClientRoleConfig clientRoleConfig;

    @PostConstruct
    void createRealm() {
        Keycloak keycloak = keycloakProvider.keycloak();
        realmConfig.initializeRealms(keycloak);
        clientConfig.initializeClients(keycloak);
        realmRolesConfig.initializeRealmRoles(keycloak);
        clientRoleConfig.initializeClientRoles(keycloak);
        keycloak.close();
    }
}