package pl.grzegorz.keycloakadapters.out.client.user;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakadapters.out.client.KeycloakProvider;
import pl.grzegorz.keycloakapplication.ports.out.user.UserEnableCommandPort;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserEnableException;
import pl.grzegorz.keycloakdomain.exception.KeycloakUserNotFoundException;
import pl.grzegorz.keycloakdomain.exception.messages.ExceptionMessage;

import javax.ws.rs.NotFoundException;

@Service
@RequiredArgsConstructor
@Slf4j
class KeycloakUserEnableCommandAdapter implements UserEnableCommandPort {

    private final KeycloakProvider keycloakProvider;

    @Value("${keycloak.moto-app-realm}")
    private String realmName;

    @Override
    public void enable(String userId) {
        try {
            var usersResource = keycloakProvider.keycloak()
                    .realm(realmName)
                    .users();
            var singleUserResource = usersResource.get(userId);
            var userRepresentation = singleUserResource.toRepresentation();
            validateEnableUser(userId, userRepresentation);
            userRepresentation.setEnabled(Boolean.TRUE);
            singleUserResource.update(userRepresentation);
        } catch (NotFoundException e) {
            log.error("Keycloak user using id -> [{}] not found", userId);
            throw new KeycloakUserNotFoundException(
                    String.format(ExceptionMessage.KEYCLOAK_USER_NOT_FOUND_ERROR.getMessage(), userId)
            );
        }
    }

    private void validateEnableUser(String userId, UserRepresentation userRepresentation) {
        if (userRepresentation.isEnabled().equals(Boolean.TRUE)) {
            throw new KeycloakUserEnableException(String.format(
                    ExceptionMessage.KEYCLOAK_USER_ENABLE_ERROR.getMessage(), userId
            ));
        }
    }
}