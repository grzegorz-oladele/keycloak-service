package pl.grzegorz.keycloakadapters.out.amqp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.grzegorz.keycloakapplication.ports.out.biker.BikerDisableCommandPort;
import pl.grzegorz.keycloakdomain.event.UserDisabledEvent;

@Service
@RequiredArgsConstructor
@Slf4j
class BikerDisableCommandPublisher implements BikerDisableCommandPort {

    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.disable.exchange}")
    private String bikerDisableExchange;
    @Value("${spring.rabbitmq.biker.disable.routing-key}")
    private String bikerDisableRoutingKey;

    @Override
    public void publish(String userId) {
        var disabledEvent = UserDisabledEvent.of(userId);
        rabbitTemplate.convertAndSend(bikerDisableExchange, bikerDisableRoutingKey, disabledEvent);
        log.info("Send event -> [{}] to exchange -> [{}]" ,disabledEvent, bikerDisableExchange);
    }
}