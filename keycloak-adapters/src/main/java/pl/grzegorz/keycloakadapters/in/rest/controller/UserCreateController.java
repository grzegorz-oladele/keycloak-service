package pl.grzegorz.keycloakadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.keycloakadapters.in.rest.api.UserCreateApi;
import pl.grzegorz.keycloakadapters.in.rest.dto.UserCreateDto;
import pl.grzegorz.keycloakapplication.ports.in.UserCreateCommandUseCase;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
class UserCreateController implements UserCreateApi {

    private final UserCreateCommandUseCase userCreateCommandUseCase;

    @Override
    public ResponseEntity<UUID> create(UserCreateDto userCreateDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(userCreateCommandUseCase.create(userCreateDto.toCreateCommand()));
    }
}