package pl.grzegorz.keycloakadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.keycloakadapters.in.rest.api.UserEnableApi;
import pl.grzegorz.keycloakapplication.ports.in.UserEnableCommandUseCase;

@RestController
@RequiredArgsConstructor
class UserEnableController implements UserEnableApi {

    private final UserEnableCommandUseCase userEnableCommandUseCase;

    @Override
    public ResponseEntity<Void> enableUser(String userId) {
        userEnableCommandUseCase.enable(userId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}