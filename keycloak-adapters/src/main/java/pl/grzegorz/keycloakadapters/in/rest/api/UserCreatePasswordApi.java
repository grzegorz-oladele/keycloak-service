package pl.grzegorz.keycloakadapters.in.rest.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.keycloakadapters.in.rest.dto.PasswordCreateDto;

@RequestMapping("/users")
public interface UserCreatePasswordApi {

    @PatchMapping("/{userId}/credentials")
    ResponseEntity<Void> assignCredentialsToUserByUserId(@PathVariable("userId") String userId,
                                                         @RequestBody @Valid PasswordCreateDto credential);
}