package pl.grzegorz.keycloakadapters.in.rest.dto;

import static pl.grzegorz.keycloakapplication.ports.in.UserCreatePasswordCommandUseCase.PasswordCreateCommand;

public record PasswordCreateDto(
        String password,
        Boolean temporary,
        UserLabel userLabel
) {

    public PasswordCreateCommand toCredentialCreateCommand() {
        return PasswordCreateCommand.builder()
                .withPassword(password)
                .withTemporary(temporary)
                .withKeycloakUserLabel(userLabel().getValue())
                .build();
    }
}