package pl.grzegorz.keycloakadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.keycloakadapters.in.rest.api.UserCreatePasswordApi;
import pl.grzegorz.keycloakadapters.in.rest.dto.PasswordCreateDto;
import pl.grzegorz.keycloakapplication.ports.in.UserCreatePasswordCommandUseCase;

@RestController
@RequiredArgsConstructor
class UserCreatePasswordController implements UserCreatePasswordApi {

    private final UserCreatePasswordCommandUseCase userCreatePasswordCommandUseCase;

    @Override
    public ResponseEntity<Void> assignCredentialsToUserByUserId(String userId, PasswordCreateDto passwordCreateDto) {
        userCreatePasswordCommandUseCase.create(userId, passwordCreateDto.toCredentialCreateCommand());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}