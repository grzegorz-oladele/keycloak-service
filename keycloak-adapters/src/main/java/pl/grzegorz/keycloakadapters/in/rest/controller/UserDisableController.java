package pl.grzegorz.keycloakadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.keycloakadapters.in.rest.api.UserDisableApi;
import pl.grzegorz.keycloakapplication.ports.in.UserDisableCommandUseCase;

@RestController
@RequiredArgsConstructor
class UserDisableController implements UserDisableApi {

    private final UserDisableCommandUseCase userDisableCommandUseCase;

    @Override
    public ResponseEntity<Void> disableUser(String userId) {
        userDisableCommandUseCase.disable(userId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}