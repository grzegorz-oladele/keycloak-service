package pl.grzegorz.keycloakadapters.in.rest.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/users")
public interface UserExistsApi {

    @GetMapping("/{userId}/exists")
    Boolean userExists(@PathVariable("userId") String userId);
}