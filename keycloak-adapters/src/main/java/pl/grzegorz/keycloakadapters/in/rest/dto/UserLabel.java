package pl.grzegorz.keycloakadapters.in.rest.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UserLabel {

    MOTO_PASSWORD("Moto_password");

    private final String value;
}