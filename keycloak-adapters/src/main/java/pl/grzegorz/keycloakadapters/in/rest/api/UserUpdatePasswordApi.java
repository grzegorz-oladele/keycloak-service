package pl.grzegorz.keycloakadapters.in.rest.api;

import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.keycloakadapters.in.rest.dto.PasswordCreateDto;

import java.util.List;

@RequestMapping("/users")
public interface UserUpdatePasswordApi {

    @PatchMapping("/{userId}/credentials/update")
    void updateUserCredentials(@PathVariable("userId") String userId,
                               @RequestBody @Valid List<PasswordCreateDto> credentials);
}