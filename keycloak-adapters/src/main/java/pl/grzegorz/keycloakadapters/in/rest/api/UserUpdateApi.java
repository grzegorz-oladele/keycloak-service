package pl.grzegorz.keycloakadapters.in.rest.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.keycloakadapters.in.rest.dto.UserUpdateDto;

@RequestMapping("/users")
public interface UserUpdateApi {

    @PatchMapping("/{userId}/update")
    ResponseEntity<Void> updateUserById(@PathVariable("userId") String userId,
                                        @RequestBody @Valid UserUpdateDto userUpdateDto);
}