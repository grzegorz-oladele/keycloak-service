package pl.grzegorz.keycloakadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.keycloakadapters.in.rest.api.UserUpdateApi;
import pl.grzegorz.keycloakadapters.in.rest.dto.UserUpdateDto;
import pl.grzegorz.keycloakapplication.ports.in.UserUpdateCommandUseCase;

@RestController
@RequiredArgsConstructor
class UserUpdateController implements UserUpdateApi {

    private final UserUpdateCommandUseCase userUpdateCommandUseCase;

    @Override
    public ResponseEntity<Void> updateUserById(String userId, UserUpdateDto userUpdateDto) {
        userUpdateCommandUseCase.update(userId, userUpdateDto.toUpdateCommand());
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}