package pl.grzegorz.keycloakadapters.in.rest.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import pl.grzegorz.keycloakadapters.in.rest.api.UserUpdatePasswordApi;
import pl.grzegorz.keycloakadapters.in.rest.dto.PasswordCreateDto;

import java.util.List;

@RestController
@RequiredArgsConstructor
class UserUpdatePasswordController implements UserUpdatePasswordApi {

    @Override
    public void updateUserCredentials(String userId, List<PasswordCreateDto> credentials) {

    }
}