package pl.grzegorz.keycloakadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/users")
public interface UserRemoveApi {

    @DeleteMapping("/{userId}/remove")
    ResponseEntity<Void> removeUser(@PathVariable("userId") String userId);
}