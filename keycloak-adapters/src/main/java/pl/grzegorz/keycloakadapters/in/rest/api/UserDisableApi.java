package pl.grzegorz.keycloakadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/users")
public interface UserDisableApi {

    @PatchMapping("/{userId}/disable")
    ResponseEntity<Void> disableUser(@PathVariable("userId") String userId);
}