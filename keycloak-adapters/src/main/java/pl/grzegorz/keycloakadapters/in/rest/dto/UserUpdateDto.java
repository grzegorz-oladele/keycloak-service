package pl.grzegorz.keycloakadapters.in.rest.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import pl.grzegorz.keycloakapplication.ports.in.UserUpdateCommandUseCase;

public record UserUpdateDto(
        @NotBlank(message = "First name must not be blank")
        String firstName,
        @NotBlank(message = "Last name must not be blank")
        String lastName,
        @NotBlank(message = "Email must not be blank")
        @Email(message = "Invalid email value")
        String email,
        @NotBlank(message = "Phone number must not be blank")
        String phoneNumber,
        @NotBlank(message = "Date of birth must not be blank")
        String dateOfBirth

) {

    public UserUpdateCommandUseCase.KeycloakUserUpdateCommand toUpdateCommand() {
        return UserUpdateCommandUseCase.KeycloakUserUpdateCommand.builder()
                .withFirstName(firstName)
                .withLastName(lastName)
                .withEmail(email)
                .withPhoneNumber(phoneNumber)
                .withDateOfBirth(dateOfBirth)
                .build();
    }
}