package pl.grzegorz.keycloakadapters.in.rest.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import static pl.grzegorz.keycloakapplication.ports.in.UserCreateCommandUseCase.UserCreateCommand;

public record UserCreateDto(
        @NotBlank(message = "Username must not be blank")
        String username,
        @NotBlank(message = "First name must not be blank")
        String firstName,
        @NotBlank(message = "Last name must not be blank")
        String lastName,
        @NotBlank(message = "Email must not be blank")
        @Email(message = "Invalid email value")
        String email,
        @NotNull(message = "Email verified must not be null")
        Boolean emailVerified,
        @NotBlank(message = "Phone number must not be blank")
        String phoneNumber,
        @NotBlank(message = "Date of birth must not be blank")
        String dateOfBirth
) {

    public UserCreateCommand toCreateCommand() {
        return UserCreateCommand.builder()
                .withUsername(username)
                .withFirstName(firstName)
                .withLastName(lastName)
                .withEmail(email)
                .withEmailVerified(emailVerified)
                .withPhoneNumber(phoneNumber)
                .withDateOfBirth(dateOfBirth)
                .build();
    }
}