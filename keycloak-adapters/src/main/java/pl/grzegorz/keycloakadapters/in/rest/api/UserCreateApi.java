package pl.grzegorz.keycloakadapters.in.rest.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.grzegorz.keycloakadapters.in.rest.dto.UserCreateDto;

import java.util.UUID;

@RequestMapping("/users")
public interface UserCreateApi {

    @PostMapping
    ResponseEntity<UUID> create(@RequestBody @Valid UserCreateDto userCreateDto);
}