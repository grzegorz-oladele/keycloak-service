package pl.grzegorz.keycloakadapters.in.rest.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/users")
public interface UserEnableApi {

    @PatchMapping("/{userId}/enable")
    ResponseEntity<Void> enableUser(@PathVariable("userId") String userId);
}