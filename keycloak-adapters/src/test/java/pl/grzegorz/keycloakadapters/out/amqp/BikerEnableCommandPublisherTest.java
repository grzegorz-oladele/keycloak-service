package pl.grzegorz.keycloakadapters.out.amqp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.keycloakdomain.event.UserEnabledEvent;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BikerEnableCommandPublisherTest {

    @InjectMocks
    private BikerEnableCommandPublisher bikerEnableCommandPublisher;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.configs[1].exchange}")
    private String enableBikerExchange;
    @Value("${spring.rabbitmq.biker.configs[1].routing-key}")
    private String enableBikerRoutingKey;

    @Test
    void shouldCallConvertAndSendOnRabbitTemplate() {
//        given
        var bikerId = UUID.randomUUID().toString();
        var enabledEvent = UserEnabledEvent.of(bikerId);
//        when
        bikerEnableCommandPublisher.publish(bikerId);
//        then
        verify(rabbitTemplate).convertAndSend(eq(enableBikerExchange), eq(enableBikerRoutingKey), eq(enabledEvent));
    }
}