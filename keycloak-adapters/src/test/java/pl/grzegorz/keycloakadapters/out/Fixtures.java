package pl.grzegorz.keycloakadapters.out;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;

import java.time.LocalDate;
import java.time.Month;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Fixtures {

    private static final UUID AGGREGATE_ID = UUID.fromString("bd4577c7-6156-4202-9b3f-56ba091561b4");
    private static final String USERNAME = "test-username";
    private static final String FIRST_NAME = "test-firstName";
    private static final String LAST_NAME = "test-lastName";
    private static final String PHONE_NUMBER = "123-456-789";
    private static final String EMAIL = "test@email.pl";
    private static final LocalDate DATE_OF_BIRTH = LocalDate.of(1990, Month.AUGUST, 23);
    private static final Boolean ENABLE = Boolean.TRUE;

    public static UserAggregate userAggregate() {
        return UserAggregate.builder()
                .withId(AGGREGATE_ID)
                .withUsername(USERNAME)
                .withFirstName(FIRST_NAME)
                .withLastName(LAST_NAME)
                .withPhoneNumber(PHONE_NUMBER)
                .withEmail(EMAIL)
                .withDateOfBirth(DATE_OF_BIRTH)
                .withEnable(ENABLE)
                .build();
    }
}