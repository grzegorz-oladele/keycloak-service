package pl.grzegorz.keycloakadapters.out.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.keycloakadapters.out.Fixtures;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.event.UserCreatedEvent;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BikerCreateCommandPublisherTest {

    @InjectMocks
    private BikerCreateCommand bikerCreateCommandPublisher;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.create.exchange}")
    private String bikerCreateExchange;
    @Value("${spring.rabbitmq.biker.create.routing-key}")
    private String bikerCreateRoutingKey;

    private UserAggregate userAggregate;

    @BeforeEach
    void setup() {
        userAggregate = Fixtures.userAggregate();
    }

    @Test
    void shouldCallConvertAndSendOnRabbitTemplate() {
//        given
        var createdEvent = UserCreatedEvent.of(userAggregate);
//        when
        bikerCreateCommandPublisher.publish(userAggregate);
//        then
        verify(rabbitTemplate).convertAndSend(eq(bikerCreateExchange), eq(bikerCreateRoutingKey), eq(createdEvent));
    }
}