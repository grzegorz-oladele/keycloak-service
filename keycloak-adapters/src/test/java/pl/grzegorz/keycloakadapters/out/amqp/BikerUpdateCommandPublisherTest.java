package pl.grzegorz.keycloakadapters.out.amqp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.keycloakadapters.out.Fixtures;
import pl.grzegorz.keycloakdomain.aggregates.UserAggregate;
import pl.grzegorz.keycloakdomain.event.UserUpdatedEvent;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BikerUpdateCommandPublisherTest {

    @InjectMocks
    private BikerUpdateCommandPublisher bikerUpdateCommandPublisher;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.update.exchange}")
    private String bikerUpdateExchange;
    @Value("${spring.rabbitmq.biker.update.routing-key}")
    private String bikerUpdateRoutingKey;

    private UserAggregate userAggregate;

    @BeforeEach
    void setup() {
        userAggregate = Fixtures.userAggregate();
    }

    @Test
    void shouldCallConvertAndSendMethodOnRabbitTemplate() {
//        given
        var bikerId = UUID.randomUUID().toString();
        var updatedEvent = UserUpdatedEvent.of(bikerId, userAggregate);
//        when
        bikerUpdateCommandPublisher.publish(bikerId, userAggregate);
//        then
        verify(rabbitTemplate).convertAndSend(eq(bikerUpdateExchange), eq(bikerUpdateRoutingKey), eq(updatedEvent));

    }
}