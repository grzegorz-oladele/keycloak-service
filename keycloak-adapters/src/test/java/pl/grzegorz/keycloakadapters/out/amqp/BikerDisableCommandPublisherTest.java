package pl.grzegorz.keycloakadapters.out.amqp;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import pl.grzegorz.keycloakdomain.event.UserDisabledEvent;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BikerDisableCommandPublisherTest {

    @InjectMocks
    private BikerDisableCommandPublisher bikerDisableCommandPublisher;
    @Mock
    private RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.biker.configs[2].exchange}")
    private String bikerDisableExchange;
    @Value("${spring.rabbitmq.biker.configs[2].routing-key}")
    private String bikerDisableRoutingKey;

    @Test
    void shouldCallConvertAndSendOnRabbitTemplate() {
//        given
        var userId = UUID.randomUUID().toString();
        var disabledEvent = UserDisabledEvent.of(userId);
//        when
        bikerDisableCommandPublisher.publish(userId);
//        then
        verify(rabbitTemplate).convertAndSend(eq(bikerDisableExchange), eq(bikerDisableRoutingKey), eq(disabledEvent));
    }
}